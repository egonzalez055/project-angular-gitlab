'use strict';

angular
  .module('hackTattooFront')
  .controller('GroupController', function ($http, $window) {

    var vm = this;
    vm.groups = [];

    vm.getGroups = function () {
   
        $http({
          method: 'get',
          url: "https://gitlab.com/api/v3/groups"
          
        }).success(function (data){
          console.log(data);
          vm.groups = data;
          console.log(vm.groups)
        })
      
    };

  });