 'use strict';

angular
  .module('hackTattooFront')
  .controller('IndexController', function ($state, $timeout, $window){

    var vm = this;
    console.log('loading index');

    $state.go('home');

    vm.home = function () {
      $state.go('home');
    };
    vm.login = function () {
      console.log('logging in');
      $timeout(function() {
        $state.go('login');
    }
        );
      $state.go('login');
    };
    vm.logout = function () {
      $window.sessionStorage.removeItem('user')
      $state.go('login');
    };

  });