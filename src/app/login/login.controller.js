'use strict';

angular
  .module('hackTattooFront')
  .controller('LoginController', function ($http, $state, API, Token) {

    var vm = this;
    vm.user = {};

    vm.access = function () {
      console.log(vm.user);
      if (vm.user.email && vm.user.password) {

        console.log('Loggeando usuario');
        $http({
          method: 'POST',
          url: "https://gitlab.com/api/v3/session?",
          data:{
            email: vm.user.email,
            password: vm.user.password
          }
        }).success(function (data){
          Token.set(data.private_token)
          console.log(data);
          console.log(data.private_token);
          $state.go('project');
        })
      }
    };

  });
