(function() {
  'use strict';

  angular
    .module('hackTattooFront')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    // $stateProvider
    //   .state('home', {
    //     url: '/',
    //     templateUrl: 'app/main/main.html',
    //     controller: 'MainController',
    //     controllerAs: 'main'
    //   });
    $stateProvider
      .state('home', {
        url: '/home',
        views: {
          'main':{
            templateUrl: 'app/home/home.html',
            controller: 'HomeController',
            controllerAs: 'home'
          }
        }
      });
    $stateProvider
      .state('login', {
        views: {
          'main':{
            templateUrl: 'app/login/login.html',
            controller: 'LoginController',
            controllerAs: 'login'
          }
        }
      });
      $stateProvider
      .state('project', {
        url: '/project',
        views: {
          'main':{
            templateUrl: 'app/project/project.html',
            controller: 'ProjectController',
            controllerAs: 'project'
          }
        }
      });
      $stateProvider
      .state('createProject', {
        views: {
          'main':{
            templateUrl: 'app/createProject/createProject.html',
            controller: 'CreateProjectController',
            controllerAs: 'createProject'
          }
        }
      });
      $stateProvider
      .state('createGroup', {
        views: {
          'main':{
            templateUrl: 'app/createGroup/createGroup.html',
            controller: 'CreateGroupController',
            controllerAs: 'createGroup'
          }
        }
      });
        $stateProvider
      .state('addMember', {
        views: {
          'main':{
            templateUrl: 'app/addMember/addMember.html',
            controller: 'AddMemberController',
            controllerAs: 'addMember'
          }
        }
      });
    $stateProvider
      .state('index', {
        url: '/',
        templateUrl: 'index.html'
      });

    $urlRouterProvider.otherwise('/home');
  }

})();
