'use strict';

angular
  .module('hackTattooFront')
  .controller('CreateProjectController', function ($http, $state, API) {

    var vm = this;
    vm.newProject = {};

    vm.create = function () {

        console.log('creating project');
        $http({
          method: 'POST',
          url: "https://gitlab.com/api/v3/projects",
          data:{
            name: vm.newProject.name,
            description: vm.newProject.description,
            public: vm.newProject.public
          }
        }).success(function (data){
          console.log(data);
          $state.go('project');
        })
      
    };

  });
