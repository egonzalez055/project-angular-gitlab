'use strict';

angular
  .module('hackTattooFront')
  .controller('CreateGroupController', function ($http, $state, API) {

    var vm = this;
    vm.newGroup = {};

    vm.create = function () {

        console.log('creating group');
        $http({
          method: 'POST',
          url: "https://gitlab.com/api/v3/groups",
          data:{
            name: vm.newGroup.name,
            description: vm.newGroup.description,
            path: vm.newGroup.path
          }
        }).success(function (data){
          console.log(data);
          $state.go('project');
        })
      
    };

  });
