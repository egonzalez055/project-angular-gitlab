'use strict';

angular
  .module('hackTattooFront')
  .controller('AddMemberController', function ($http, $state, API) {

    var vm = this;
    vm.member = {};

    vm.goBack = function () {
      $state.go('project');
    };

  vm.permission = {
        'owner': '50',
        'master': '40',
        'developer': '30',
        'reporter': '20',
        'guest': '10'
      };


    vm.add = function () {

        console.log('adding member');
        $http({
          method: 'POST',
          url: "https://gitlab.com/api/v3/groups/"+ vm.member.group_id + "/members",
          data:{
            user_id: vm.member.member_id,
            access_level: vm.permission[vm.member.access_level]
          }
        }).success(function (data){
          console.log(data);
          $state.go('project');
        })
      
    };

  });
