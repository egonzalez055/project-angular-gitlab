'use strict';

angular
  .module('hackTattooFront')
  .service('Token', function ($window) {

    this.set = function (users) {
      $window.sessionStorage.user = users;
    };

    this.get = function () {
      return JSON.parse($window.sessionStorage.getItem('user'));
    };

    this.has = function () {
      return !!$window.sessionStorage.getItem('user');
    };

  });