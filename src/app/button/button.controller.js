'use strict';

angular
  .module('hackTattooFront')
  .controller('ButtonController', function ($http, $window, $state) {

    var vm = this;

    vm.getGroups = function () {
        $state.go('group');
    };

    vm.getProjects = function () {
        $state.go('project');
    };



  });