'use strict';

angular
  .module('hackTattooFront')
  .controller('ProjectController', function ($http, $state) {

    var vm = this;
    vm.projects = [];
    vm.groups = [];
    vm.members = [];
    vm.branches = [];
    vm.files = [];

    vm.permission = {
      '50': 'Owner',
      '40': 'Master',
      '30': 'Developer',
      '20': 'Reporter',
      '10': 'Guest'
    };

    vm.createProject = function () {
      $state.go('createProject')
    };

    vm.createGroup = function () {
      $state.go('createGroup')
    };

    vm.getProjects = function () {
      //console.log(vm.projects);
      

        //console.log('getting projects');
        $http({
          method: 'get',
          url: "https://gitlab.com/api/v3/projects"
          
        }).success(function (data){
          console.log(data);
          vm.projects = data;
          console.log(vm.projects)
        })
      
    };

     vm.getGroups = function () {
   
        $http({
          method: 'get',
          url: "https://gitlab.com/api/v3/groups"
          
        }).success(function (data){
          console.log(data);
          vm.groups = data;
          console.log(vm.groups)
        })
      
    };

     vm.deleteProject = function (id) {
   
        $http({
          method: 'delete',
          url: "https://gitlab.com/api/v3/projects/" + id
          
        }).success(function (data){
          console.log(data);
          console.log("has been deleted");
         
         
        })
      
    };

    vm.deleteGroup = function (id) {
   
        $http({
          method: 'delete',
          url: "https://gitlab.com/api/v3/groups/" + id
          
        }).success(function (data){
          console.log(data);
          console.log("has been deleted");
         
         
        })
      
    };

    vm.groupMembers = function (id) {
   
        $http({
          method: 'get',
          url: "https://gitlab.com/api/v3/groups/"+id+"/members"  
          
        }).success(function (data){
          vm.members = data;
          console.log(data);
          console.log(vm.members);
          
         
         
        })
      
    };

    vm.groupAddMember = function () {
      $state.go('addMember');
    };

    vm.getBranches = function (id) {
       
            $http({
              method: 'get',
              url: "https://gitlab.com/api/v3/projects/"+id+"/repository/branches"  
              
            }).success(function (data){
              vm.branches = data;
              console.log(data);
              console.log(vm.branches);
              
             
             
            })
          
   };

  vm.getFiles = function (id) {
       
            $http({
              method: 'get',
              url: "https://gitlab.com/api/v3/projects/"+id+"/repository/tree"  
              
            }).success(function (data){
              vm.files = data;

              console.log(data);
              console.log(vm.files);
              
             
             
            })
          
   };

  });
